window.addEventListener('load', () => {

  // INTERACTIVE ELEMENTS

  let pageCarousel = document.getElementById('pageCarousel');
  let items = pageCarousel.querySelectorAll('.page-carousel__item');
  let sections = new Array(items.length);

  applyForEachElement(items, (item, i) => {
    sections[i] = document.getElementById(
      item.getAttribute('data-section-id')
    );
  });


  // EVENT LISTENERS

  if (pageCarousel) {
    applyForEachElement(items, item => {
      item.onclick = (e) => {
        section = document.getElementById(
          e.target.getAttribute('data-section-id')
        );
        section.scrollIntoView();
      };
    });

    let mainLoop = setInterval(
      setCurrentActiveItem,
      250
    );
  }


  // UTILS

  function applyForEachElement(elems, func) {
    for (let i = 0; i < elems.length; i++) {
      func(elems[i], i);
    }
  };

  function setCurrentActiveItem() {
    // if page carousel is deleted
    if (!pageCarousel) {
      clearInterval(mainLoop);
      return;
    }
    let found = false;
    let newActiveItem;
    for (let i = 0; i < sections.length; i++) {
      if (isCenterInView(sections[i]) && !found) {
        newActiveItem = items[i];
        found = true;
        break;
      }
    }
    if (newActiveItem) {
      for (let i = 0; i < items.length; i++) {
        disactivate(items[i]);
      }
      activate(newActiveItem);
    }
  }

  function isCenterInView(elem) {
    let center = getOffsetY(elem) + elem.clientHeight * 0.5;
    let viewTop = window.pageYOffset;
    let viewBottom = window.pageYOffset + window.innerHeight;

    return viewTop <= center && center < viewBottom;
  }

  function getOffsetY(elem) {
      let rect = elem.getBoundingClientRect();
      let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      return rect.top + scrollTop;
  }

  function activate(elem) {
    elem.classList.add('active');
  }

  function disactivate(elem) {
    elem.classList.remove('active');
  }

  function isActive(elem) {
    elem.classList.contains('active');
  }


});
