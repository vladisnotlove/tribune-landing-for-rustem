window.addEventListener('load', () => {


  // INTERACTIVE ELEMENTS

  let mainCTAbtn = document.getElementById('mainCTAbtn');
  let CTAbtns = document.getElementsByClassName('color__CTA-btn');

  let orderFormSection = document.getElementById('orderFormSection');
  let orderForm = document.getElementById('orderForm');

  let colors = orderForm.querySelectorAll('.order-form__color');
  let pictures = orderForm.querySelectorAll('.order-form__picture');


  // EVENT LISTENERS

  mainCTAbtn.onclick = (e) => {
    orderFormSection.scrollIntoView();
  };

  applyForEachElement(CTAbtns, (btn, i) => {
    let color = document.getElementById(btn.getAttribute('data-color-id'));
    btn.onclick = (e) => {
      color.click();
      orderFormSection.scrollIntoView();
    }
  });


  applyForEachElement(colors, (color, i) => {
    let picture = document.getElementById(color.getAttribute('data-picture-id'));
    color.onclick = (e) => {
      disactivateAll(colors);
      activate(e.currentTarget);
      disactivateAll(pictures);
      activate(picture);
    };
  });


  // UTILS

  function applyForEachElement(elems, func) {
    for (let i = 0; i < elems.length; i++) {
      func(elems[i], i);
    }
  };

  function activate(elem) {
    elem.classList.add('active');
  };

  function disactivateAll(elems) {
    for (let i = 0; i < elems.length; i++) {
      elems[i].classList.remove('active');
    }
  };

});
