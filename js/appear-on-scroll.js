window.addEventListener('load', () => {

  // INTERACTIVE ELEMENTS

  let notAppearedElements = document.body.querySelectorAll('[data-appering]');

  // EVENT LISTENERS

  let mainLoop = setInterval(
    showElementsWasInView,
    200
  );

  // UTILS

  function showElementsWasInView() {
    notAppearedNum = 0;
    for (let i = 0; i < notAppearedElements.length; i++) {
      let elem = notAppearedElements[i];
      if (elem && wasTopInView(elem)) {
        appearingTime = elem.getAttribute('data-appering-time') || 0;
        setTimeout(() => {show(elem)}, appearingTime);
        notAppearedElements[i] = undefined;
      } else {
        notAppearedNum++;
      }
    }
    if (notAppearedNum == 0) {
      clearInterval(mainLoop);
    }
  }

  function wasCenterInView(elem) {
    let center = getOffsetY(elem) + elem.clientHeight * 0.5;
    let viewBottom = window.pageYOffset + window.innerHeight;

    return center < viewBottom;
  }

  function wasTopInView(elem) {
    let top = getOffsetY(elem)  + elem.clientHeight * 0.1;
    let viewBottom = window.pageYOffset + window.innerHeight;

    return top < viewBottom;
  }

  function getOffsetY(elem) {
      let rect = elem.getBoundingClientRect();
      let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      return rect.top + scrollTop;
  }

  function show(elem) {
    elem.classList.remove('hidden-nice');
    elem.classList.add('show-nice');
  };

});
