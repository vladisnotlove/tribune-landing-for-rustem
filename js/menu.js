window.addEventListener('load', () => {

  // INTERACTIVE ELEMENTS

  let menuBtn = document.getElementById('menuBtn');
  let openMenuIcon = document.getElementById('openMenuIcon');
  let closeMenuIcon = document.getElementById('closeMenuIcon');
  let menu = document.getElementById('menu');


  // EVENT LISTENERS

  menuBtn.onclick = (e) => {
    showOrHide(menu);
    showOrHide(openMenuIcon);
    showOrHide(closeMenuIcon);
  };


  // UTILS

  function showOrHide(elem) {
    elem.classList.toggle('hidden');
    elem.classList.toggle('show');

    if (!elem.classList.contains('show') && !elem.classList.contains('hidden')) {
      elem.classList.add('show');
    }
  };

  function show(elem) {
    elem.classList.remove('hidden');
    elem.classList.add('show');
  };

  function hide(elem) {
    elem.classList.remove('show');
    elem.classList.add('hidden');
  };

  function isHidden(elem) {
    elem.classList.contains('hidden');
  };

  function isParentAndChild(elemA, elemB) {
    let parentB = elemB.parentElement;
    let found = parentB == elemA;
    while (!found && parentB != null) {
      parentB = parentB.parentElement;
      found = parentB == elemA;
    }
    return found;
  }

  function focusOn(elem) {
    elem.focus();
  };

});
