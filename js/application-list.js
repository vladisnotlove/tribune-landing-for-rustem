window.addEventListener('load', () => {

  // INTERACTIVE ELEMENTS

  let applicationList = document.getElementById('applicationList');
  let applications = applicationList.querySelectorAll('.application-list__item');


  // EVENT LISTENERS

  applyForEachElement(applications, (application, i) => {

    application.onclick = (e) => {
      e.preventDefault();
      let lastI = applications.length-1;
      let prevApplication = i == 0 ? applications[lastI] : applications[i-1];
      let nextApplication = i == lastI ? applications[0] : applications[i+1];
      setPrev(prevApplication);
      setActive(application);
      setNext(nextApplication);
    };

  });


  // UTILS

  function setActive(elem) {
    elem.classList.add('active');
    elem.classList.remove('next');
    elem.classList.remove('prev');
  }

  function setPrev(elem) {
    elem.classList.add('prev');
    elem.classList.remove('next');
    elem.classList.remove('active');
  }

  function setNext(elem) {
    elem.classList.add('next');
    elem.classList.remove('active');
    elem.classList.remove('prev');
  }

  function applyForEachElement(elems, func) {
    for (let i = 0; i < elems.length; i++) {
      func(elems[i], i);
    }
  };

});
