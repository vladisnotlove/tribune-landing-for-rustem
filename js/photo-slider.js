window.addEventListener('load', () => {

  // INTERACTIVE ELEMENTS

  let photoSlider = document.getElementById('photoSlider');
  let nextBtn = photoSlider.querySelector('.photo-slider__next-btn');
  let prevBtn = photoSlider.querySelector('.photo-slider__prev-btn');
  let visibleArea = photoSlider.querySelector('.photo-slider__visible-area');
  let photosList = photoSlider.querySelector('.photo-slider__list');


  // EVENT LISTENERS

  let averageWidth = getChildrenWidthSum(photosList) / photosList.children.length;

  nextBtn.onclick = () => {
    visibleArea.scrollBy({
      top: 0,
      left: averageWidth,
      behavior: 'smooth'
    });
  };

  prevBtn.onclick = () => {
    visibleArea.scrollBy({
      top: 0,
      left: -averageWidth,
      behavior: 'smooth'
    });
  };



  // UTILS

  function getChildrenWidthSum(elem) {
    let sum = 0;
    applyForEachElement(elem.children, (elem) => {
      sum += elem.offsetWidth;
    });
    return sum;
  }

  function applyForEachElement(elems, func) {
    for (let i = 0; i < elems.length; i++) {
      func(elems[i], i);
    }
  };

});
