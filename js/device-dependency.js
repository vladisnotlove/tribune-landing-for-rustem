window.addEventListener('load', () => {

  // INTERACTIVE ELEMENTS

  let pcOnlys = document.querySelectorAll('[data-pc-only]');


  // EVENT LISTENERS

  if (pcOnlys && isMobile()) {
    for (let i = 0; i < pcOnlys.length; i++) {
      pcOnlys[i].remove();
    }
  }

  // UTILS

  function isMobile() {
    return (navigator.userAgent.match(/Android/i)
      || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPad/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/BlackBerry/i)
      || navigator.userAgent.match(/Windows Phone/i));
  }

});
